import { createAsyncThunk } from '@reduxjs/toolkit';
import { createTask, getTasks, updateTask } from '../../services/tasks';
import { Task } from '../../types/tasks';

export const createTaskAsync = createAsyncThunk(
  'tasks/createTaskAsync',
  async (data: Omit<Task, 'id'>) => {
    const res = await createTask(data);
    return res;
  }
);

export const updateTaskAsync = createAsyncThunk(
  'tasks/updateTaskAsync',
  async (data: { id: number; values: Omit<Task, 'id'> }) => {
    const res = await updateTask(data.id, data.values);
    return res;
  }
);

export const fetchTasksAsync = createAsyncThunk(
  'tasks/fetchTasksAsync',
  async () => {
    return getTasks();
  }
);
