import { PayloadAction, createSlice } from '@reduxjs/toolkit';
import { Task } from '../../types/tasks';
import {
  createTaskAsync,
  fetchTasksAsync,
  updateTaskAsync,
} from './tasksAsync';

export interface TasksState {
  tasks: Task[];
  loading: boolean;
}

const initialState: TasksState = {
  tasks: [],
  loading: false,
};

export const tasksSlice = createSlice({
  name: 'tasks',
  initialState,
  reducers: {
    setTasks: (state, action: PayloadAction<Task[]>) => {
      state.tasks = action.payload;
    },
    addTask: (state, action: PayloadAction<Task>) => {
      state.tasks.push(action.payload);
    },
    removeTask: (state, action: PayloadAction<{ id: number }>) => {
      state.tasks = state.tasks.filter((t) => t.id !== action.payload.id);
    },
  },
  extraReducers: (builder) => {
    builder.addCase(createTaskAsync.pending, (state) => {
      state.loading = true;
    });
    builder.addCase(createTaskAsync.rejected, (state) => {
      state.loading = false;
    });
    builder.addCase(createTaskAsync.fulfilled, (state, action) => {
      state.tasks.push(action.payload);
      state.loading = false;
    });
    builder.addCase(fetchTasksAsync.fulfilled, (state, action) => {
      state.tasks = action.payload;
    });
    builder.addCase(updateTaskAsync.fulfilled, (state, action) => {
      state.tasks = state.tasks.map((t) => {
        if (t.id === action.meta.arg.id) {
          return {
            ...t,
            ...action.meta.arg.values,
          };
        }
        return t;
      });
    });
  },
});

export const { addTask, removeTask, setTasks } = tasksSlice.actions;

export default tasksSlice.reducer;
