export interface Product {
  id: number;
  name: string;
  description: string;
}

export type ProductFormValues = Omit<Product, 'id'>;
