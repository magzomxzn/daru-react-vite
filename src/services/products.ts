import { Product, ProductFormValues } from '../types/products';
import { instance } from './httpClient';

export const getProducts = ({ s }: { s?: string }) => {
  return instance
    .get<Product[]>(`/products`, {
      params: {
        s,
      },
    })
    .then((r) => r.data);
};

export const getProduct = (id: number) => {
  return instance.get<Product>(`/products/${id}`).then((r) => r.data);
};

export const createProduct = (data: ProductFormValues) => {
  return instance.post<Product>(`/products`, data).then((r) => r.data);
};

export const updateProduct = (id: number, data: ProductFormValues) => {
  return instance.put<Product>(`/products/${id}`, data).then((r) => r.data);
};
