import { Task } from '../types/tasks';
import { instance } from './httpClient';

export const getTasks = () => {
  return instance.get<Task[]>(`/tasks`).then((r) => r.data);
};

export const getTask = (id: number) => {
  return instance.get<Task>(`/tasks/${id}`).then((r) => r.data);
};

export const createTask = (data: Omit<Task, 'id'>) => {
  return instance.post<Task>(`/tasks`, data).then((r) => r.data);
};

export const updateTask = (id: number, data: Omit<Task, 'id'>) => {
  return instance.put<unknown>(`/tasks/${id}`, data).then((r) => r.data);
};

export const deleteTask = (id: number) => {
  return instance.delete<Task>(`/tasks/${id}`).then((r) => r.data);
};
