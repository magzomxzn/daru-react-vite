import React, { ChangeEvent, FormEvent, useEffect, useState } from 'react';

import { useUserData } from '../../../providers/UserDataProvider';
import { Product, ProductFormValues } from '../../../types/products';
import { Profile } from '../../profile/Profile';
import './ProductForm.scss';

const initialValues: ProductFormValues = {
  name: '',
  description: '',
};

type Props = {
  updateProduct?: Product;
  isLoading?: boolean;
  onSubmit: (data: ProductFormValues) => void;
};

export const ProductForm: React.FC<Props> = ({
  updateProduct,
  isLoading,
  onSubmit,
}) => {
  const { profile } = useUserData();

  const [formValues, setFormValues] =
    useState<ProductFormValues>(initialValues);

  useEffect(() => {
    setFormValues(updateProduct || initialValues);
  }, [updateProduct]);

  const handleSubmit = (e: FormEvent) => {
    e.preventDefault();
    console.log(formValues);
    if (formValues.name) {
      onSubmit(formValues);
      handleClear();
    }
  };

  const handleFormChange = (e: ChangeEvent<HTMLInputElement>) => {
    setFormValues((v) => ({ ...v, [e.target.name]: e.target.value }));
  };

  const handleClear = () => {
    setFormValues(initialValues);
  };

  const titleError = formValues.name ? '' : 'Required';

  return (
    <div className="ProductForm">
      {profile && <Profile user={profile} />}
      <form onSubmit={handleSubmit}>
        <div className="formControl">
          <input
            name="name"
            value={formValues.name}
            placeholder="Title"
            onChange={handleFormChange}
          />
          {Boolean(titleError) && <div className="error">{titleError}</div>}
        </div>
        <div className="formControl">
          <input
            name="description"
            value={formValues.description}
            placeholder="Description"
            onChange={handleFormChange}
          />
        </div>
        <button type="submit" disabled={isLoading}>
          Submit
        </button>
        <button type="button" onClick={handleClear}>
          Clear
        </button>
      </form>
    </div>
  );
};
