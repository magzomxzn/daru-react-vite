import React from 'react';

import './ProductItem.css';

export const ProductItem: React.FC<{
  title: string;
  description: string;
  isSelected: boolean;
  onCardClicked: () => void;
}> = ({ title, description, isSelected, onCardClicked }) => {
  const handleCardClick = () => {
    onCardClicked();
  };

  return (
    <div className="ProductItem" onClick={handleCardClick}>
      <div className="ProductItem__title">{title}</div>
      <div className="ProductItem__description">{description}</div>
      {isSelected && (
        <div className="ProductItem__cart-state">Added to cart</div>
      )}
    </div>
  );
};
