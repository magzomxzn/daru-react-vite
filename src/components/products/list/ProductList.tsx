import { Product } from '../../../types/products';
import { ProductItem } from '../item/ProductItem';

import './ProductList.css';

export const ProductList: React.FC<{
  data: Product[];
  selectedIds: number[];
  onItemClicked: (id: number) => void;
}> = ({ data, selectedIds, onItemClicked }) => {
  return (
    <div className="ProductList">
      {data.map((product) => (
        <ProductItem
          key={product.id}
          title={product.name}
          description={product.description}
          isSelected={selectedIds.includes(product.id)}
          onCardClicked={() => {
            onItemClicked(product.id);
          }}
        />
      ))}
    </div>
  );
};
