import { Box } from "@mui/material";
import React from "react";
import { ChatMessage as IChatMessage } from "../../types/chat";
import { ChatMessage } from "./ChatMessage";

type Props = {
  messages: IChatMessage[];
};
export const ChatList: React.FC<Props> = ({ messages }) => {
  return (
    <Box display={"flex"} flexDirection={"column"}>
      {messages.map((message, idx) => (
        <ChatMessage key={idx} message={message} />
      ))}
    </Box>
  );
};
