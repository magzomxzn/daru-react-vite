import React, { useEffect, useRef, useState } from "react";
import { Socket, io } from "socket.io-client";
import { useUserData } from "../../providers/UserDataProvider";
import { ChatMessage } from "../../types/chat";
import { ChatForm } from "./ChatForm";
import { ChatList } from "./ChatList";
import { ChatWrapper, MessageList } from "./chat-styles";

const API_HOST = "https://dar-u-group1.dar-dev.zone";

export const Chat: React.FC = () => {
  const { profile } = useUserData();

  const [messages, setMessages] = useState<ChatMessage[]>([]);

  const socketRef = useRef<Socket>();

  useEffect(() => {
    const socket = io(`${API_HOST}/chats/1`, {
      transports: ["websocket", "polling"],
    });

    socket.on("chat message", (msg: string) => {
      console.log("New message", msg);
      try {
        const message = JSON.parse(msg.split("]")[1]?.trim());
        setMessages((v) => [...v, message]);
      } catch (e) {
        console.error(e);
      }
    });

    socketRef.current = socket;

    return () => {
      socket.close();
    };
  }, []);

  const handleMessageSend = (message: string) => {
    if (!profile) {
      return;
    }
    const messageObj: ChatMessage = {
      username: `${profile.firstName} ${profile.lastName}`,
      message,
      date: new Date().toISOString(),
    };
    socketRef.current?.emit("chat message", JSON.stringify(messageObj));
  };

  return (
    <ChatWrapper>
      <MessageList>
        <ChatList messages={messages} />
      </MessageList>
      <ChatForm onMessageSubmit={handleMessageSend} />
    </ChatWrapper>
  );
};
