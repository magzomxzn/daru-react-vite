import React, { useState } from "react";
import SendIcon from "../../assets/send.svg?react";
import {
  ChatFormContainer,
  ChatFormInput,
  ChatFormToolbar,
  ChatFormWrapper,
  SendButton,
} from "./chat-styles";

type Props = {
  onMessageSubmit: (v: string) => void;
};

export const ChatForm: React.FC<Props> = ({ onMessageSubmit }) => {
  const [text, setText] = useState("");

  const handleMessageSubmit = () => {
    if (!text?.trim()) {
      return;
    }
    onMessageSubmit(text);
    setText("");
  };

  return (
    <ChatFormContainer>
      <ChatFormWrapper>
        <ChatFormInput
          placeholder="Добавить комментарий..."
          value={text}
          onChange={(e) => setText(e.target.value)}
        />
        <ChatFormToolbar>
          <SendButton variant="contained" onClick={handleMessageSubmit}>
            <SendIcon />
          </SendButton>
        </ChatFormToolbar>
      </ChatFormWrapper>
    </ChatFormContainer>
  );
};
