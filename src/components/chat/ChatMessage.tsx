import { Box } from "@mui/material";
import React from "react";
import { ChatMessage as IChatMessage } from "../../types/chat";
import { MessageDate, MessageText, Username } from "./chat-styles";

export const ChatMessage: React.FC<{ message: IChatMessage }> = ({
  message,
}) => {
  return (
    <Box marginBottom={"12px"}>
      <Box
        display={"flex"}
        columnGap={"8px"}
        marginBottom={"4px"}
        alignItems={"center"}
        padding={"12px 16px 0"}
      >
        <Username>{message.username}</Username>
        <MessageDate>{new Date(message.date).toLocaleDateString()}</MessageDate>
      </Box>
      <MessageText>{message.message}</MessageText>
    </Box>
  );
};
