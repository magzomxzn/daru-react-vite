import { Button, TextField, Typography, styled } from "@mui/material";

export const ChatWrapper = styled("div")`
  display: flex;
  flex-direction: column;
  background: #f7f8fc;
  height: 780px;
  width: 504px;
  border: 1px solid #e0e3ea;
`;

export const MessageList = styled("div")`
  flex: 1;
`;

export const ChatFormInput = styled(TextField)`
  & input {
    padding: 0;
    font-size: 14px;
    font-style: normal;
    font-weight: 400;
    line-height: 18px;
  }
  & fieldset {
    border: 0;
  }
`;

export const ChatFormContainer = styled("div")<{ mode?: "light" | "dark" }>`
  padding: 12px 24px 24px;
  background: ${(props) => (props.mode === "dark" ? "black" : "#F7F8FC")};
`;

export const ChatFormWrapper = styled("div")`
  padding: 12px 16px;
  border-radius: 4px;
  background: #fff;
  display: flex;
  flex-direction: column;
`;

export const ChatFormToolbar = styled("div")`
  margin-top: 12px;
  padding-top: 12px;
  border-top: 1px solid #e0e3ea;
  display: flex;
  justify-content: flex-end;
`;

export const SendButton = styled(Button)`
  background: #ced7df;
  width: 32px;
  height: 32px;
  padding: 4px;
  min-width: unset;
`;

export const Username = styled(Typography)`
  font-size: 14px;
  font-style: normal;
  font-weight: 400;
  line-height: 18px;
`;

export const MessageDate = styled(Typography)`
  font-size: 12px;
  font-style: normal;
  font-weight: 400;
  line-height: 16px;
  color: #8a96a1;
`;

export const MessageText = styled("div")`
  font-size: 14px;
  font-style: normal;
  font-weight: 400;
  line-height: 18px;
  color: #465564;
  padding: 0 16px;
`;
