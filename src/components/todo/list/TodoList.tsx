import React from 'react';

import './TodoList.scss';
import { TodoItem } from '../item/TodoItem';
import { Task } from '../../../types/tasks';

export const TodoList: React.FC<{
  tasks: Task[];
}> = ({ tasks }) => {
  return (
    <div className="TodoList">
      {tasks.map((task) => (
        <TodoItem task={task} key={task.id} />
      ))}
    </div>
  );
};
