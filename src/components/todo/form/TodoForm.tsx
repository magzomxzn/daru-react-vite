import React, { useState } from 'react';

import './TodoForm.scss';

type Props = {
  onCreate: (title: string) => void;
};

export const TodoForm: React.FC<Props> = ({ onCreate }) => {
  const [value, setValue] = useState('');

  const handleSubmit = () => {
    if (!value?.trim()) {
      return;
    }
    onCreate(value);
  };

  return (
    <div className="TodoForm">
      <input value={value} onChange={(e) => setValue(e.target.value)} />
      <button onClick={handleSubmit}>Add</button>
    </div>
  );
};
