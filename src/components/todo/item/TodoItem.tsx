import React, { useState } from 'react';

import { useAppDispatch } from '../../../state/store';
import { updateTaskAsync } from '../../../state/tasks/tasksAsync';
import { removeTask } from '../../../state/tasks/tasksReducer';
import { Task } from '../../../types/tasks';
import './TodoItem.scss';

type Props = {
  task: Task;
};
export const TodoItem: React.FC<Props> = ({ task }) => {
  const dispatch = useAppDispatch();

  const [isChecked, setIsChecked] = useState(task.isDone);

  const handleDelete = () => {
    // delete task
    dispatch(removeTask({ id: task.id }));
  };

  const handleTaskStatusChange = () => {
    setIsChecked(!isChecked);
    // update isDone flag
    dispatch(
      updateTaskAsync({
        id: task.id,
        values: {
          title: task.title,
          isDone: !isChecked,
        },
      })
    );
  };

  return (
    <div className="TodoItem">
      <input
        className="checkbox"
        type="checkbox"
        checked={isChecked}
        onChange={handleTaskStatusChange}
      />
      <div className="title">{task.title}</div>
      <button onClick={handleDelete}>Delete</button>
    </div>
  );
};
