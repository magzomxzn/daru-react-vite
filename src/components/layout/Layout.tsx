import React from "react";
import { Header } from "./header/Header";

import { Outlet } from "react-router-dom";
import "./Layout.scss";

const ENV_NAME = import.meta.env.VITE_ENV_NAME;

export const Layout: React.FC = () => {
  return (
    <div className="Layout">
      <Header />
      <div className="Main">
        <h2>App environment is {ENV_NAME}</h2>
        <Outlet />
      </div>
    </div>
  );
};
