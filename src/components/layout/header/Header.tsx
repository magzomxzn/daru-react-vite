import React from 'react';
import { Link } from 'react-router-dom';

import './Header.scss';
import { Profile } from '../../profile/Profile';
import { useUserData } from '../../../providers/UserDataProvider';

export const Header: React.FC = () => {
  const { profile } = useUserData();
  const theme = 'light';
  
  return (
    <div className={theme === 'light' ? 'Header' : 'Header-dark'}>
      <nav>
        <Link to="/">Main</Link>
        <Link to="/products">Product</Link>
        <Link to="/todo">Tasks</Link>
        <Link to="/chat">Chat</Link>
      </nav>
      {profile ? <Profile user={profile} /> : null}
    </div>
  );
};
