import React from 'react';
import { UserProfile } from '../../types/user';

import './Profile.scss';

export const Profile: React.FC<{ user: UserProfile }> = ({
  user: { firstName, lastName, avatar },
}) => {
  return (
    <div className="Profile">
      <img src={avatar} alt="userAvatar" className="userAvatar" />
      <div className="userInfo">
        <div className="userName">{firstName}</div>
        <div className="userName">{lastName}</div>
      </div>
    </div>
  );
};
