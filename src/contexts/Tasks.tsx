import { Dispatch, createContext } from 'react';
import { Task } from '../types/tasks';

export interface TasksState {
  tasks: Task[];
}

export const initialState: TasksState = {
  tasks: [],
};

export const tasksReducer = (
  prevState: TasksState,
  action: TaskAction
): TasksState => {
  switch (action.type) {
    case TasksActionTypes.CREATE:
      return {
        ...prevState,
        tasks: [
          ...prevState.tasks,
          { id: Math.random() * 1000, ...action.payload },
        ],
      };
    case TasksActionTypes.DELETE:
      return {
        ...prevState,
        tasks: prevState.tasks.filter((task) => task.id !== action.payload.id),
      };
    default:
      return prevState;
  }
};

export enum TasksActionTypes {
  CREATE = 'create',
  DELETE = 'delete',
}

export interface TaskActionCreate {
  type: TasksActionTypes.CREATE;
  payload: Pick<Task, 'title' | 'isDone'>;
}

export interface TaskActionDelete {
  type: TasksActionTypes.DELETE;
  payload: Pick<Task, 'id'>;
}

export type TaskAction = TaskActionCreate | TaskActionDelete;

export const actionTaskCreate = (
  payload: Pick<Task, 'title' | 'isDone'>
): TaskActionCreate => {
  return {
    type: TasksActionTypes.CREATE,
    payload,
  };
};

export const actionTaskDelete = (
  payload: Pick<Task, 'id'>
): TaskActionDelete => {
  return {
    type: TasksActionTypes.DELETE,
    payload,
  };
};

export interface TasksContextType {
  tasks: Task[];
  dispatch: Dispatch<TaskAction>;
}
export const TasksContext = createContext<TasksContextType>({
  tasks: [],
  dispatch: () => '',
});
