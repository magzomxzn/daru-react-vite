import React from 'react';
import { UserProfile } from '../types/user';

export interface UserData {
  profile: UserProfile | null;
  changeName: (data: { firstName: string; lastName: string }) => void;
}

const defaultValues: UserData = {
  profile: null,
  changeName: () => '',
};

export const UserDataContext = React.createContext<UserData>(defaultValues);
