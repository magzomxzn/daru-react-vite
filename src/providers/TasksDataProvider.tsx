import React, { PropsWithChildren, useContext, useReducer } from 'react';
import { TasksContext, initialState, tasksReducer } from '../contexts/Tasks';

export const TasksDataProvider: React.FC<PropsWithChildren> = ({
  children,
}) => {
  const [state, dispatch] = useReducer(tasksReducer, initialState);

  return (
    <TasksContext.Provider
      value={{
        tasks: state.tasks,
        dispatch,
      }}
    >
      {children}
    </TasksContext.Provider>
  );
};

export const useTasksContext = () => {
  return useContext(TasksContext);
};
