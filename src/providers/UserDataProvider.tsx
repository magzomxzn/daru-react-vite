import { PropsWithChildren, useContext, useState } from 'react';
import { UserDataContext } from '../contexts/UserData';
import { UserProfile } from '../types/user';

export const UserDataProvider: React.FC<PropsWithChildren> = ({ children }) => {
  const [profile, setProfile] = useState<UserProfile>({
    id: 123,
    firstName: 'Student',
    lastName: '123',
    avatar: 'https://placekitten.com/640/360',
  });

  const changeName = (data: Pick<UserProfile, 'firstName' | 'lastName'>) => {
    setProfile((v) => ({
      ...v,
      firstName: data.firstName,
      lastName: data.lastName,
    }));
  };

  return (
    <UserDataContext.Provider value={{ profile, changeName }}>
      {children}
    </UserDataContext.Provider>
  );
};

export const useUserData = () => {
  return useContext(UserDataContext);
};
