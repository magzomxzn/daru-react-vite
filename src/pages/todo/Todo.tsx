import { useQuery } from '@tanstack/react-query';
import React, { useEffect } from 'react';
import { TodoForm } from '../../components/todo/form/TodoForm';
import { TodoList } from '../../components/todo/list/TodoList';
import { getProducts } from '../../services/products';
import { useAppDispatch, useAppSelector } from '../../state/store';
import { createTaskAsync, fetchTasksAsync } from '../../state/tasks/tasksAsync';

export const Todo: React.FC = () => {
  const dispatch = useAppDispatch();
  const { tasks } = useAppSelector((s) => s.tasks);

  const { data: products, isLoading } = useQuery(['products', 'list', 'todo'], {
    queryFn: async () => {
      const products = await getProducts({});
      return products;
    },
    cacheTime: 30000,
    staleTime: 10000,
  });

  console.log(products, isLoading);

  useEffect(() => {
    // fetch tasks
    dispatch(fetchTasksAsync());
  }, [dispatch]);

  const handleCreate = (title: string) => {
    // create task
    dispatch(createTaskAsync({ title, isDone: false }));
  };

  return (
    <div>
      <TodoForm onCreate={handleCreate} />
      <TodoList tasks={tasks} />
    </div>
  );
};
