import { useQuery } from '@tanstack/react-query';
import { useState } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import { ProductList } from '../../components/products/list/ProductList';
import { getProducts } from '../../services/products';

export const Products = () => {
  // const queryClient = useQueryClient();
  const [search, setSearch] = useState('');

  console.log(search);
  const {
    data: products,
    isLoading,
    refetch,
  } = useQuery(['products', 'list', search], {
    queryFn: async () => {
      const products = await getProducts({ s: search });
      return products;
    },
    placeholderData: [],
    cacheTime: 60000,
    staleTime: 30000,
  });

  const navigate = useNavigate();

  const handleItemClick = async (id: number) => {
    navigate(`/products/${id}`);
  };

  const handleRefetch = () => {
    refetch();
    // queryClient.invalidateQueries(['products']);
  };

  return (
    <>
      <Link to="/products/create">New Product</Link>

      <div>
        <button onClick={handleRefetch}>Refetch</button>
        <input
          type="text"
          value={search}
          onChange={(e) => setSearch(e.target.value)}
        />
        {isLoading ? (
          <h2>Loading</h2>
        ) : (
          <ProductList
            data={products || []}
            selectedIds={[]}
            onItemClicked={handleItemClick}
          />
        )}
      </div>
    </>
  );
};
