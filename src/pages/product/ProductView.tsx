import React, { useEffect, useState } from 'react';
import { Link, useParams } from 'react-router-dom';
import { getProduct } from '../../services/products';
import { Product } from '../../types/products';

export const ProductView: React.FC = () => {
  const [product, setProduct] = useState<Product>();
  const [loading, setLoading] = useState(true);

  const { id } = useParams();

  useEffect(() => {
    if (!id) {
      return;
    }
    setLoading(true);
    getProduct(+id)
      .then((r) => {
        setProduct(r);
      })
      .catch((e) => {
        console.error(e);
      })
      .finally(() => {
        setLoading(false);
      });
  }, [id]);

  if (loading) {
    return <h1>Loading</h1>;
  }

  if (!product) {
    return <h1>No product with id #{id}</h1>;
  }

  return (
    <div>
      <Link to={`/products/${id}/edit`}>Edit</Link>
      <h1>Product with ID: #{product.id}</h1>
      <h2>Name: {product.name}</h2>
      <div>Description: {product.description}</div>
    </div>
  );
};
