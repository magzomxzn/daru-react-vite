import React, { useEffect, useState } from 'react';
import { ProductForm } from '../../components/products/form/ProductForm';
import { Product, ProductFormValues } from '../../types/products';
import { getProduct, updateProduct } from '../../services/products';
import { useNavigate, useParams } from 'react-router-dom';

export const ProductEdit: React.FC = () => {
  const navigate = useNavigate();
  const { id } = useParams();

  const [product, setProduct] = useState<Product>();
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    if (!id) {
      return;
    }
    setLoading(true);
    getProduct(+id)
      .then((r) => {
        setProduct(r);
      })
      .catch((e) => {
        console.error(e);
      })
      .finally(() => {
        setLoading(false);
      });
  }, [id]);

  const handleFormSubmit = async (data: ProductFormValues) => {
    if (!id) {
      return;
    }
    await updateProduct(+id, data);
    navigate('/products');
  };

  if (loading) {
    return <h1>Loading</h1>;
  }

  if (!product) {
    return <h1>No product with id #{id}</h1>;
  }

  return <ProductForm updateProduct={product} onSubmit={handleFormSubmit} />;
};
