import { useMutation, useQueryClient } from '@tanstack/react-query';
import React from 'react';
import { useNavigate } from 'react-router-dom';
import { ProductForm } from '../../components/products/form/ProductForm';
import { createProduct } from '../../services/products';
import { Product, ProductFormValues } from '../../types/products';

export const ProductCreate: React.FC = () => {
  const navigate = useNavigate();
  const queryClient = useQueryClient();
  const { mutate, isLoading } = useMutation({
    mutationFn: (data: ProductFormValues) => createProduct(data),
    onSuccess: (product) => {
      queryClient.setQueryData<Product[]>(['products', 'list'], (prev) => {
        return prev ? [...prev, product] : [product];
      });
      navigate('/products');
    },
  });

  const handleFormSubmit = async (data: ProductFormValues) => {
    mutate(data);
  };

  return <ProductForm onSubmit={handleFormSubmit} isLoading={isLoading} />;
};
