import { QueryClient, QueryClientProvider } from '@tanstack/react-query';
import { Provider } from 'react-redux';
import { RouterProvider, createBrowserRouter } from 'react-router-dom';
import './App.css';
import { Layout } from './components/layout/Layout';
import { ProductCreate } from './pages/product/ProductCreate';
import { ProductEdit } from './pages/product/ProductEdit';
import { ProductView } from './pages/product/ProductView';
import { Products } from './pages/product/Products';
import { Todo } from './pages/todo/Todo';
import { UserDataProvider } from './providers/UserDataProvider';
import { store } from './state/store';
import { ChatPage } from './pages/chat/ChatPage';

const queryClient = new QueryClient({
  defaultOptions: {
    queries: {
      refetchOnWindowFocus: false,
    },
  },
});

const router = createBrowserRouter([
  {
    path: '/',
    element: <Layout />,
    children: [
      {
        path: '/products',
        element: <Products />,
      },
      {
        path: '/products/create',
        element: <ProductCreate />,
      },
      {
        path: '/products/:id',
        element: <ProductView />,
      },
      {
        path: '/products/:id/edit',
        element: <ProductEdit />,
      },
      {
        path: '/todo',
        element: <Todo />,
      },
      {
        path: '/chat',
        element: <ChatPage />
      }
    ],
  },
]);

function App() {
  return (
    <QueryClientProvider client={queryClient}>
      <Provider store={store}>
        <UserDataProvider>
          <div>
            <RouterProvider router={router} />
          </div>
        </UserDataProvider>
      </Provider>
    </QueryClientProvider>
  );
}

export default App;
